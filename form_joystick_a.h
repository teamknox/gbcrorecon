/*

 FORM_JOYSTICK_A.H

 Map Include File.

 Info:
   Section       : 
   Bank          : 0
   Map size      : 20 x 18
   Tile set      : bkg_joystick.gbr
   Plane count   : 1 plane (8 bits)
   Plane order   : Tiles are continues
   Tile offset   : 0
   Split data    : No

 This file was generated by GBMB v1.8

*/

#define form_joystick_aWidth 20
#define form_joystick_aHeight 18
#define form_joystick_aBank 0

extern unsigned char form_joystick_a[];

/* End of FORM_JOYSTICK_A.H */
