# LEGO Mindstroms controller by Gameboy color

## How to use
### Operation mode
You can select two types of controlling for MindStorms. One is "Command Mode". Other one is "Direct input" as 'One hand operation'. 
<table>
<tr>
<td><img src="./pics/rorecon1.jpg"></td>
<td><img src="./pics/rorecon2.jpg"></td>
</tr>
</table>


### Included genuine remote controller features
To equip Piano Playing as same as RCX CC also LEGO genuine remote contoller by LEGO Corp. 
<table>
<tr>
<td><img src="./pics/rorecon3.jpg"></td>
<td><img src="./pics/rorecon4.jpg"></td>
</tr>
</table>

## How it works
Compatible with RCX command center is based on a same protocol of LEGO genuine remote controller.


# License
Copyright (c) Osamu OHASHI  
Distributed under the MIT License either version 1.0 or any later version. 
