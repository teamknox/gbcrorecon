/*

 FORM_JOYSTICK.H

 Map Include File.

 Info:
   Section       : 
   Bank          : 0
   Map size      : 20 x 18
   Tile set      : bkg_joystick.gbr
   Plane count   : 1 plane (8 bits)
   Plane order   : Tiles are continues
   Tile offset   : 0
   Split data    : No

 This file was generated by GBMB v1.8

*/

#define form_joystickWidth 20
#define form_joystickHeight 18
#define form_joystickBank 0

extern unsigned char form_joystick[];

/* End of FORM_JOYSTICK.H */
