;***********************************************************
;*                                                         *
;*              IR DRIVER for Color Game Boy               *
;*                [ Ir-Receiver for RCX ]                  *
;*                                                         *
;*                        2000/03                          *
;*                   K.I. of TeamKNOx                      *
;*                                                         *
;*---------------------------------------------------------*
;* - Protocol : 2400bps/ 8bit/ Parity Odd/ Stopbit 1       *
;* - Carrier Frequency : 38KHz                             *
;* - Input : Non                                           *
;* - Onput : Return Value(1 byte)                          *
;*                 0x00: Received                          *
;*                 0x01: Receive error                     *
;*                 0x02: Parity error                      *
;*                 0x03: Data length error                 *
;*           Recieve data(1 byte) ---> rcv_buff            *
;***********************************************************
;4.194304MHz / 2400bps = 1747.6clock

	.area	_BSS
_rcv_buff::			;receive data buffer
	.ds	1

	.area	_CODE
_ir_rcv::
	di
	ldh	a,(#0x56)
	or	#0b11000000
	ldh	(#0x56),a	;Read Enable

;*** Start Bit ***
	ld	c,#<3
1$:	ld	b,#0xFF
2$:	ldh	a,(#0x56)				;12
	and	#0x02		;receive check		; 8
	jr	z,3$					;12
	dec	b					; 4
	jr	nz,2$					;12
	dec	c
	jr	nz,1$
	jr	rcv_err1	;not received
3$:	ld	b,#<150;151				; 8
	call	wait					;12+16*b+12

;*** Data Bit ***
	ld	d,#<0		;Data Clear		; 8
	ld	e,#<0		;Parity Clear		; 8
	ld	c,#<8		;8 bit Counter		; 8
rcv_data:
	call	rcv_bit					;12+172
	ld	b,a					; 4
	or	d		;Data | rcv_bit		; 4
	rrca			;Data>>1		; 4
	ld	d,a					; 4
	ld	a,b					; 4
	xor	e					; 4
	ld	e,a					; 4
	ld	b,#<91;93				; 8
	call	wait					;12+16*b+12
	dec	c					; 4
	jr	nz,rcv_data				;12

;*** Parity Bit ***
	call	rcv_bit					;12+172
	xor	e					; 4
	jr	z,rcv_err2	;Parity error		; 8
	ld	b,#<93;95				; 8
	call	wait					;12+16*b+12

;*** Stop Bit ***
	call	rcv_bit
	cp	#0x01
	jr	nz,rcv_err3	;Data length error

	ld	e,#0x00		;Nomal ended
	jr	rcv_end

rcv_err1:
	ld	e,#0x01		;Receive error
	jr	rcv_end

rcv_err2:
	ld	e,#0x02		;Parity error
	jr	rcv_end

rcv_err3:
	ld	e,#0x03		;Data length error

rcv_end:
	ld	a,d
	ld	(#_rcv_buff),a	;Data --> (_rcv_buff)
	ei
	ret

;=================================================
; rcv_bit
;	input		:non
;	output		:a
;	destroyed	:b
;-------------------------------------------------
; Sampling Pitch for 28.5kHz to 57kHz Carrier Cut
;	:4.194304MHz/38kHz/2*75% = 41.4clock
;=================================================
rcv_bit:
	ldh	a,(#0x56)	;12
	ld	b,a		;	 4
	ldh	a,(#0x56)	;	12
	ldh	a,(#0x56)	;	12
	ldh	a,(#0x56)	;	12
	and	b		; 4
	ld	b,a		; 4
	ldh	a,(#0x56)	;12
	ldh	a,(#0x56)	;12
	ldh	a,(#0x56)	;12
	and	b		;	 4
	ld	b,a		;	 4
	ldh	a,(#0x56)	;	12
	ldh	a,(#0x56)	;	12
	ldh	a,(#0x56)	;	12
	and	b		; 4
	rrca			; 4
	and	#0x01		; 8
	ret			;16
		;--------------------------
		;total clock	172

;=================================================
; wait
;	input		:b
;	output		:non
;	destroyed	:b
;=================================================
wait:
	dec	b		; 4
	jr	nz,wait		;12	-4
	ret			;	16
		;--------------------------
		;total clock 	 16	12

;end of ir_rcv.s
