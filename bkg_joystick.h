/*

 BKG_JOYSTICK.H

 Include File.

 Info:
  Form                 : All tiles as one unit.
  Format               : Gameboy 4 color.
  Compression          : None.
  Counter              : None.
  Tile size            : 8 x 8
  Tiles                : 0 to 127

  Palette colors       : Included.
  SGB Palette          : None.
  CGB Palette          : 1 Byte per entry.

  Convert to metatiles : No.

 This file was generated by GBTD v2.2

*/


/* Bank of tiles. */
#define bkg_joystickBank 0

/* Super Gameboy palette 0 */
#define bkg_joystickSGBPal0c0 0
#define bkg_joystickSGBPal0c1 8
#define bkg_joystickSGBPal0c2 25368
#define bkg_joystickSGBPal0c3 0

/* Super Gameboy palette 1 */
#define bkg_joystickSGBPal1c0 4104
#define bkg_joystickSGBPal1c1 6
#define bkg_joystickSGBPal1c2 0
#define bkg_joystickSGBPal1c3 0

/* Super Gameboy palette 2 */
#define bkg_joystickSGBPal2c0 4104
#define bkg_joystickSGBPal2c1 6
#define bkg_joystickSGBPal2c2 0
#define bkg_joystickSGBPal2c3 0

/* Super Gameboy palette 3 */
#define bkg_joystickSGBPal3c0 4104
#define bkg_joystickSGBPal3c1 6
#define bkg_joystickSGBPal3c2 0
#define bkg_joystickSGBPal3c3 0

/* Gameboy Color palette 0 */
#define bkg_joystickCGBPal0c0 24311
#define bkg_joystickCGBPal0c1 32767
#define bkg_joystickCGBPal0c2 15855
#define bkg_joystickCGBPal0c3 0

/* Gameboy Color palette 1 */
#define bkg_joystickCGBPal1c0 24311
#define bkg_joystickCGBPal1c1 31
#define bkg_joystickCGBPal1c2 15855
#define bkg_joystickCGBPal1c3 0

/* Gameboy Color palette 2 */
#define bkg_joystickCGBPal2c0 24311
#define bkg_joystickCGBPal2c1 32767
#define bkg_joystickCGBPal2c2 15855
#define bkg_joystickCGBPal2c3 15855

/* Gameboy Color palette 3 */
#define bkg_joystickCGBPal3c0 24311
#define bkg_joystickCGBPal3c1 32767
#define bkg_joystickCGBPal3c2 15855
#define bkg_joystickCGBPal3c3 31

/* Gameboy Color palette 4 */
#define bkg_joystickCGBPal4c0 24311
#define bkg_joystickCGBPal4c1 32767
#define bkg_joystickCGBPal4c2 15855
#define bkg_joystickCGBPal4c3 31744

/* Gameboy Color palette 5 */
#define bkg_joystickCGBPal5c0 24311
#define bkg_joystickCGBPal5c1 992
#define bkg_joystickCGBPal5c2 15855
#define bkg_joystickCGBPal5c3 0

/* Gameboy Color palette 6 */
#define bkg_joystickCGBPal6c0 6108
#define bkg_joystickCGBPal6c1 8935
#define bkg_joystickCGBPal6c2 6596
#define bkg_joystickCGBPal6c3 6368

/* Gameboy Color palette 7 */
#define bkg_joystickCGBPal7c0 6108
#define bkg_joystickCGBPal7c1 8935
#define bkg_joystickCGBPal7c2 6596
#define bkg_joystickCGBPal7c3 6368
/* CGBpalette entries. */
extern unsigned char bkg_joystickCGB[];
/* Start of tile array. */
extern unsigned char bkg_joystick[];

/* End of BKG_JOYSTICK.H */
