/****************************************************************************
 *  RoReCon                                                                 *
 *  2000/03                                                                 *
 *  TeamKNOx                                                                *
 ****************************************************************************/
#include <gb.h>
#include <stdlib.h>
#include "bkg.h"
#include "obj.h"

/* extern */
extern UBYTE rcv_buff;
extern UBYTE nvm_status;
extern UBYTE nvm_drive_mode;
extern UBYTE nvm_motor_sel[2];
extern UBYTE nvm_motor_dir[2];
extern UBYTE nvm_motor_pwr;
extern UBYTE nvm_task_num;

/* definition */
#define MODE_JOYSTICK	0
#define MODE_PIANO	1
#define MODE_GENUINE	2
#define FLAG_OFF	0
#define FLAG_ON		1
#define OFF		0
#define ON		1
#define MOVEMENT	1
#define SELECTION	2
#define TASK		3
#define DRIVE_LR	0
#define DRIVE_DS	1
#define MOTOR_A		0x01
#define MOTOR_B		0x02
#define MOTOR_C		0x04
#define REVERSE		0x00
#define FORWARD		0x80
#define MOTOR_ON	0x80
#define MOTOR_OFF	0x40
#define F3		175
#define FS3		185
#define G3		196
#define GS3		208
#define A3		220
#define AS3		233
#define B3		247
#define C4		262
#define CS4		277
#define D4		294
#define DS4		311
#define E4		330
#define F4		350
#define FS4		370
#define G4		392
#define GS4		416
#define A4		440
#define AS4		466
#define B4		494
#define C5		523
#define CS5		554
#define D5		587
#define DS5		622
#define E5		660
#define F5		699
#define FS5		741
#define G5		785
#define GS5		832
#define A5		880
#define AS5		932
#define B5		988
#define C6		1047
#define R		65535
#define L1		160
#define L2H		120
#define L2		80
#define L4H		60
#define L4		40
#define L8H		30
#define L8		20
#define L16H		15
#define L16		10

/* character data */
#include "bkg.c"
#include "bkg_joystick.c"
#include "bkg_piano.c"
#include "bkg_genuine.c"
#include "obj.c"
UWORD bkg_palette[] = {
    bkgCGBPal0c0, bkgCGBPal0c1, bkgCGBPal0c2, bkgCGBPal0c3,
    bkgCGBPal1c0, bkgCGBPal1c1, bkgCGBPal1c2, bkgCGBPal1c3,
    bkgCGBPal2c0, bkgCGBPal2c1, bkgCGBPal2c2, bkgCGBPal2c3,
    bkgCGBPal3c0, bkgCGBPal3c1, bkgCGBPal3c2, bkgCGBPal3c3,
    bkgCGBPal4c0, bkgCGBPal4c1, bkgCGBPal4c2, bkgCGBPal4c3,
    bkgCGBPal5c0, bkgCGBPal5c1, bkgCGBPal5c2, bkgCGBPal5c3,
    bkgCGBPal6c0, bkgCGBPal6c1, bkgCGBPal6c2, bkgCGBPal6c3,
    bkgCGBPal7c0, bkgCGBPal7c1, bkgCGBPal7c2, bkgCGBPal7c3,
};

UWORD obj_palette[] = {
    objCGBPal0c0, objCGBPal0c1, objCGBPal0c2, objCGBPal0c3,
    objCGBPal1c0, objCGBPal1c1, objCGBPal1c2, objCGBPal1c3
};

/* tile map data */
#include "form_joystick.c"
#include "form_joystick_a.c"
#include "form_piano.c"
#include "form_piano_a.c"
#include "form_genuine.c"
#include "form_genuine_a.c"
unsigned char *tile[] = { bkg_joystick, bkg_piano, bkg_genuine };
unsigned char *form[] = { form_joystick, form_piano, form_genuine };
unsigned char *form_a[] = { form_joystick_a, form_piano_a, form_genuine_a };

/* button */
unsigned char btn_break1[] = { 0x10,0x11,0x12,0x13 };
unsigned char btn_break2[] = { 0x14,0x15,0x16,0x17 };
unsigned char btn_press1[] = { 0x18,0x19,0x1A,0x1B };
unsigned char btn_press2[] = { 0x1C,0x1D,0x1E,0x1F };
unsigned char btn2_break[] = { 47 };
unsigned char btn2_press[] = { 48 };

/* selection bkg_palette */
unsigned char sel_off[] = { 2, 2, 2, 2, 2, 2, 2, 2 };
unsigned char sel_on[]  = { 0, 0, 0, 0, 0, 0, 0, 0 };
unsigned char pwr_off[] = { 0, 0, 0, 0, 0, 0, 0, 0 };
unsigned char pwr_on[]  = { 1, 1, 1, 1, 1, 1, 1, 1 };
unsigned char black[]   = { 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0 };
unsigned char white[]   = { 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0 };
unsigned char red1[]    = { 1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1 };
unsigned char red3[]    = { 3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3 };
unsigned char blue[]    = { 4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4 };
unsigned char key_off[] = { 0, 0, 0, 0 };
unsigned char key_on[]  = { 5, 5, 5, 5 };

/* message */
unsigned char msg_default[]     = { "Default Data Set" };
unsigned char msg_pusha[]     = { "PUSH A" };
unsigned char msg_d_mode[][3] = { "LR","DS" };
unsigned char msg_clr[]       = { "                        " };
unsigned char msg_err1[]      = { "Warning!TwoMotorOverlap " };
unsigned char msg_err2[]      = { "Sorry.. Mode D-SCant Use" };
unsigned char msg_err3[]      = { "Sorry.. RecordinCant Use" };
unsigned char msg_rcx[][16]   = { "RCX say 'Roger!'","RCX say '-----'" };
unsigned char msg_song[][9]   = { "StarWars","        ","        " };
unsigned char msg_credit[]    = { "RoReCon 00/03/19TeamKNOx" };

/* RCX Command */
UBYTE set_motor_dir[]   = { 0xE1, 1 };
UBYTE set_motor_onoff[] = { 0x21, 1 };
UBYTE set_motor_power[] = { 0x13, 3 };
UBYTE start_task[]      = { 0x71, 1 };
UBYTE stop_task[]       = { 0x81, 1 };
UBYTE play_tone[]       = { 0x23, 3 };
UBYTE remocon[]         = { 0xD2, 2 };

/* remocon parameter */
UWORD para[] = { 0x0001, 0x0002, 0x0004, 0x0008, 0x0010, 0x0020, 0x0040, 0x0080,
                 0x0100, 0x0200, 0x0400, 0x0800, 0x1000, 0x2000, 0x4000, 0x8000,
                 0x0000, 0x0000, 0x0000 };

/* RCX Piano data */
UWORD black_freq[] = { FS3,GS3,AS3,0,CS4,DS4,0,FS4,GS4,AS4,0,CS5,DS5,0,FS5,GS5,AS5 };
UWORD white_freq[] = { F3,G3,A3,B3,C4,D4,E4,F4,G4,A4,B4,C5,D5,E5,F5,G5,A5,B5 };
UWORD song1[] = { G4,L4/3, G4,L4/3, G4,L4/3, C5,L4/3, R,L4/3, C5,L4/3, G5,L2,
                  F5,L4/3, E5,L4/3, D5,L4/3, C6,L2, G5,L4,
                  F5,L4/3, E5,L4/3, D5,L4/3, C6,L2, G5,L4,
                  F5,L4/3, E5,L4/3, F5,L4/3, D5,L2H, 0};	/* star wars */
UWORD song2[] = { 0 };
UWORD song3[] = { 0 };

/* work area */
UBYTE cursor_x;
UBYTE cursor_y;
UBYTE cursor_max_x;
UBYTE cursor_max_y;
UBYTE cursor_off_x;
UBYTE cursor_off_y;
UBYTE cursor_add_x;
UBYTE cursor_add_y;
UBYTE key;
UBYTE keycode_flag;
UBYTE keycode_pre;
UBYTE keycode;
UBYTE rsv_data[7];
UBYTE tglbit3;
UBYTE drive_mode;
UBYTE motor_sel[2];
UBYTE motor_dir[2];
UBYTE motor_pwr;
UBYTE motor_pwr_old;
UBYTE task_num;

/* functoins */
void init_character()
{
  set_bkg_data( 0, 128, bkg );
  set_sprite_data( 0, 12, obj );
  set_bkg_palette( 0, 8, bkg_palette );
  set_sprite_tile( 0, 0 );
  set_sprite_tile( 1, 1 );
  set_sprite_tile( 2, 2 );
  set_sprite_tile( 3, 3 );
  set_sprite_palette( 0, 2, obj_palette );
  set_sprite_prop( 0, 0 );
  set_sprite_prop( 1, 0 );
  set_sprite_prop( 2, 0 );
  set_sprite_prop( 3, 0 );
  SHOW_BKG;
  SHOW_SPRITES;
  DISPLAY_ON;
  enable_interrupts();
}

void cls()
{
  UBYTE y;
  for ( y=0; y<18; y++ ) {
    set_bkg_tiles( 0, y, 20, 1, msg_clr );
    *(UBYTE *)0xFF4F = 1;			/* select palette bank */
    set_bkg_tiles( 0, y, 20, 1, white );
    *(UBYTE *)0xFF4F = 0;			/* select charater bank */
  }
}

void init_cursor( max_x, max_y, off_x, off_y, add_x, add_y )
{
  cursor_max_x = max_x;
  cursor_max_y = max_y;
  cursor_off_x = off_x;
  cursor_off_y = off_y;
  cursor_add_x = add_x;
  cursor_add_y = add_y;
  keycode_pre = 0;
  keycode = 0;
}

void move_cursor()
{
  UBYTE x, y;
  key = joypad();
  if ( keycode_flag == FLAG_OFF ) {
    if ( key & (J_UP|J_DOWN|J_LEFT|J_RIGHT|J_A) ) {
      keycode_flag = FLAG_ON;
    }
    if ( key & J_UP ) {
      if( cursor_y > 0 )                 cursor_y--;
    } else if( key & J_DOWN ) {
      if( cursor_y < (cursor_max_y-1) )  cursor_y++;
    } else if( key & J_LEFT ) {
      if( cursor_x > 0 )                 cursor_x--;
    } else if( key & J_RIGHT ) {
      if( cursor_x < (cursor_max_x-1) )  cursor_x++;
    } else if( key & J_A ) {
      keycode_pre |= J_A;
    }
  } else {
    if ( !(key & (J_UP|J_DOWN|J_LEFT|J_RIGHT|J_A)) ) {
      keycode_flag = FLAG_OFF;
    }
    if ( keycode_pre & J_A ) {
      if ( !(key & J_A) ) {
        keycode_pre &= ~J_A;
        keycode |= J_A;
      }
    }
  }
  x = cursor_off_x + cursor_add_x * cursor_x;
  y = cursor_off_y + cursor_add_y * cursor_y;
  move_sprite( 0, x,   y   );
  move_sprite( 1, x,   y+8 );
  move_sprite( 2, x+8, y   );
  move_sprite( 3, x+8, y+8 );
}

void move_cursor2( UBYTE n, UBYTE x, UBYTE y )
{
  move_sprite( n,   x,   y   );
  move_sprite( n+1, x,   y+8 );
  move_sprite( n+2, x+8, y   );
  move_sprite( n+3, x+8, y+8 );
}

void press_button( UBYTE x, UBYTE y )
{
  set_bkg_tiles( x,   y,   3, 1, btn_press1 );
  set_bkg_tiles( x,   y+1, 1, 1, &btn_press1[3] );
  set_bkg_tiles( x+2, y+1, 1, 1, btn_press2 );
  set_bkg_tiles( x,   y+2, 3, 1, &btn_press2[1] );
}

void break_button( UBYTE x, UBYTE y )
{
  set_bkg_tiles( x,   y,   3, 1, btn_break1 );
  set_bkg_tiles( x,   y+1, 1, 1, &btn_break1[3] );
  set_bkg_tiles( x+2, y+1, 1, 1, btn_break2 );
  set_bkg_tiles( x,   y+2, 3, 1, &btn_break2[1] );
}

void press_key_b( UBYTE x )
{
  UBYTE i;
  for ( i=4; i<7; i++ ) {
    set_sprite_tile( i, 5 );
    set_sprite_prop( i, 1 );
    move_sprite( i, x*8+22, i*8+16 );
  }
}

void break_key_b( UBYTE x )
{
  UBYTE i;
  for ( i=4; i<7; i++ ) {
    set_sprite_tile( i, 4 );
    set_sprite_prop( i, 1 );
    move_sprite( i, x*8+22, i*8+16 );
  }
}
void press_key_w( UBYTE x )
{
  *(UBYTE *)0xFF4F = 1;			/* select palette bank */
  set_bkg_tiles( x+1, 4, 1, 4, key_on );
  *(UBYTE *)0xFF4F = 0;			/* select charater bank */
}

void break_key_w( UBYTE x )
{
  *(UBYTE *)0xFF4F = 1;			/* select palette bank */
  set_bkg_tiles( x+1, 4, 1, 4, key_off );
  *(UBYTE *)0xFF4F = 0;			/* select charater bank */
}

void disp_value( UWORD d, UWORD e, UBYTE c, UBYTE x, UBYTE y )
{
  UWORD m;
  UBYTE i, n;
  unsigned char data[6];

  m = 1;
  if ( c > 1 ) {
    for ( i=1; i<c; i++ ) {
      m = m * e;
    }
  }
  for ( i=0; i<c; i++ ) {
    n = d / m; d = d % m; m = m / e;
    data[i] = '0' + n;    /* '0' - '9' */
    if ( data[i] > '9' )  data[i] += 7;
  }
  set_bkg_tiles( x, y, c, 1, data );
}

void disp_message( unsigned char *msg, unsigned char *color )
{
  set_bkg_tiles( 11, 14, 8, 3, msg );
  *(UBYTE *)0xFF4F = 1;			/* select palette bank */
  set_bkg_tiles( 11, 14, 8, 3, color );
  *(UBYTE *)0xFF4F = 0;			/* select charater bank */
}

void disp_joystick()
{
  set_bkg_tiles( 11, 7, 1, 2, msg_d_mode[drive_mode] );
  disp_value( task_num, 10, 1, 2, 15 );
  *(UBYTE *)0xFF4F = 1;			/* select palette bank */
  set_bkg_tiles( 11, 4, 8, 1, sel_off );
  set_bkg_tiles( drive_mode*5+11, 4, 3, 1, sel_on );
  set_bkg_tiles( 13, 7, 3, 2, sel_off );
  set_bkg_tiles( (motor_sel[0]>>1)+13, 7, 1, 1, sel_on );
  set_bkg_tiles( (motor_sel[1]>>1)+13, 8, 1, 1, sel_on );
  set_bkg_tiles( 17, 7, 2, 2, sel_off );
  set_bkg_tiles( (motor_dir[0]>>7)+17, 7, 1, 1, sel_on );
  set_bkg_tiles( (motor_dir[1]>>7)+17, 8, 1, 1, sel_on );
  set_bkg_tiles( 12, 11, 7, 1, pwr_off );
  set_bkg_tiles( 12, 11, motor_pwr, 1, pwr_on );
  *(UBYTE *)0xFF4F = 0;			/* select charater bank */
}

void rcx_cntl( UBYTE *cmd, UBYTE p1, UBYTE p2, UBYTE p3, UBYTE mode )
{
  UBYTE snd_data[16];
  UBYTE rsv_flag;
  UBYTE len, chksum, i, j;

  if ( mode != MODE_GENUINE ) tglbit3 ^= 0x08;

/* Header */
  snd_data[0] = 0x55;
  snd_data[1] = 0xFF;
  snd_data[2] = 0x00;

/* Command */
  snd_data[3] = *cmd;
  if ( mode != MODE_GENUINE ) snd_data[3] |= tglbit3;
  snd_data[4] = ~snd_data[3];
  chksum = snd_data[3];
  cmd++;
  len = *cmd;
  i = 5;

/* Parameter */ 
    if ( len > 0 ) {
      snd_data[i] = p1;
      snd_data[i+1] = ~snd_data[i];
      chksum += snd_data[i];
      i++;
      i++;
    }
    if ( len > 1 ) {
      snd_data[i] = p2;
      snd_data[i+1] = ~snd_data[i];
      chksum += snd_data[i];
      i++;
      i++;
    }
    if ( len > 2 ) {
      snd_data[i] = p3;
      snd_data[i+1] = ~snd_data[i];
      chksum += snd_data[i];
      i++;
      i++;
    }

/* Checksum */ 
  snd_data[i] = chksum;
  snd_data[i+1] = ~chksum;
  i++;
  i++;

/* Ir send */
  for ( j=0; j<i; j++ ) {
    ir_snd( snd_data[j] );
  }

/* rcx msg */
  if ( mode != MODE_GENUINE ) {
    for ( i=0; i<5; i++ ) {
      rsv_flag = ir_rcv();
    }
    if( (rsv_flag==0) && (snd_data[3]==rcv_buff) ) {
        set_bkg_tiles( 11, 15, 8, 2, msg_rcx[0] );
    } else {
        set_bkg_tiles( 11, 15, 8, 2, msg_rcx[1] );
    }
  }
}

UBYTE select_mode( UBYTE mode )
{
  UBYTE save_x, i;

  if ( mode != MODE_GENUINE ) {
    disp_message( msg_credit, blue );
    for ( i=12; i<16; i++ ) {
      set_sprite_tile( i, 4 );
      set_sprite_prop( i, 1 );
    }
  }
  save_x = cursor_x;
  init_cursor( 1, 2, 13, 30, 0, 0 );
  cursor_x = cursor_y = 0;
  while ( cursor_y == 0 ) {
    move_cursor();
    if ( keycode & J_A ) {
      keycode &= ~J_A;
      if ( mode < 2 ) mode++;
      else            mode=0;
      cursor_x = 0;
      return( mode );
    }
  }
  if ( mode != MODE_GENUINE ) disp_message( msg_clr, black );
  return( mode );
}

UBYTE mode_joystick( UBYTE mode )
{
  UBYTE data[16];
  UBYTE x, y, pad, oldpad;
  UBYTE section, save_x, save_y;
  UBYTE movelock = OFF;

  disp_joystick();
  while ( 1 ) {
    mode = select_mode( mode );
    if ( mode != MODE_JOYSTICK )  return( mode );
    cursor_y = 1;
    save_y = cursor_y;
    save_x = cursor_x;
    while ( cursor_y > 0 ) {
      if ( cursor_x > 2 ) cursor_x = 2;
// Movement
      while ( (cursor_y==1) && (cursor_x==0) ) {
        section = MOVEMENT;
        init_cursor( 2, 3, 46, 79, 0, 0 );
        *(UBYTE *)0xFF4F = 1;			/* select palette bank */
        for ( y=0; y<3; y++ ) {
          for ( x=0; x<3; x++ ) {
            set_bkg_tiles( x*3+2, y*3+4, 1, 1, white );
          }
        }
        *(UBYTE *)0xFF4F = 0;			/* select charater bank */
        move_cursor();
        if ( joypad() & J_SELECT ) {
          movelock = ON;
          waitpadup();
        }
        while ( (joypad()&J_A) | (movelock == ON) ) {
          *(UBYTE *)0xFF4F = 1;			/* select palette bank */
          for ( y=0; y<3; y++ ) {
            for ( x=0; x<3; x++ ) {
              set_bkg_tiles( x*3+2, y*3+4, 1, 1, red1 );
            }
          }
          *(UBYTE *)0xFF4F = 0;			/* select charater bank */
          if ( joypad() & J_SELECT ) {
            movelock = OFF;
            waitpadup();
          }
          if ( drive_mode == DRIVE_DS ) {
            disp_message( msg_err2, red3 );
            break;
          }
          if ( motor_sel[0] == motor_sel[1] ) {
            disp_message( msg_err1, red3 );
            break;
          }
          pad = joypad();
          x=4,y=6;
          while ( pad & 0x1F ) {
          pad = joypad();
          if ( pad != oldpad ) {
            break_button( x, y );
            oldpad = pad;
            switch ( pad & 0x0F ) {
              case 0x00:
                x=4,y=6;
                break;
              case 0x01:
                x=4+3,y=6;
                break;
              case 0x02:
                x=4-3,y=6;
                break;
              case 0x04:
                x=4,y=6-3;
                break;
              case 0x05:
                x=4+3,y=6-3;
                break;
              case 0x06:
                x=4-3,y=6-3;
                break;
              case 0x08:
                x=4,y=6+3;
                break;
              case 0x09:
                x=4+3,y=6+3;
                break;
              case 0x0A:
                x=4-3,y=6+3;
                break;
            }
            press_button( x, y );
            move_cursor2( 0, x*8+14, y*8+31 );
            if ( motor_pwr != motor_pwr_old ) {
              motor_pwr_old = motor_pwr;
              rcx_cntl( set_motor_power, MOTOR_A|MOTOR_B|MOTOR_C, 0x02, motor_pwr, mode );
            }
            switch ( pad & 0x0F ) {
              case 0x00: /* Center */
                rcx_cntl( set_motor_onoff, MOTOR_OFF|motor_sel[0]|motor_sel[1], 0, 0, mode );
                disp_message( msg_clr, black );
                break;
              case 0x01: /* Right */
                rcx_cntl( set_motor_dir, motor_dir[0]|motor_sel[0], 0, 0, mode );
                rcx_cntl( set_motor_dir, motor_dir[1]^0x80|motor_sel[1], 0, 0, mode );
                rcx_cntl( set_motor_onoff, MOTOR_ON|motor_sel[0]|motor_sel[1], 0, 0, mode );
                break;
              case 0x02: /* Left */
                rcx_cntl( set_motor_dir, motor_dir[0]^0x80|motor_sel[0], 0, 0, mode );
                rcx_cntl( set_motor_dir, motor_dir[1]|motor_sel[1], 0, 0, mode );
                rcx_cntl( set_motor_onoff, MOTOR_ON|motor_sel[0]|motor_sel[1], 0, 0, mode );
                break;
              case 0x04: /* Up */
                rcx_cntl( set_motor_dir, motor_dir[0]|motor_sel[0], 0, 0, mode );
                rcx_cntl( set_motor_dir, motor_dir[1]|motor_sel[1], 0, 0, mode );
                rcx_cntl( set_motor_onoff, MOTOR_ON|motor_sel[0]|motor_sel[1], 0, 0, mode );
                break;
              case 0x05: /* UpRight */
                rcx_cntl( set_motor_onoff,MOTOR_OFF|motor_sel[1], 0, 0, mode );
                rcx_cntl( set_motor_dir, motor_dir[0]|motor_sel[0], 0, 0, mode );
                rcx_cntl( set_motor_onoff, MOTOR_ON|motor_sel[0], 0, 0, mode );
                break;
              case 0x06: /* UpLeft */
                rcx_cntl( set_motor_onoff, MOTOR_OFF|motor_sel[0], 0, 0, mode );
                rcx_cntl( set_motor_dir, motor_dir[1]|motor_sel[1], 0, 0, mode );
                rcx_cntl( set_motor_onoff, MOTOR_ON|motor_sel[1], 0, 0, mode );
                break;
              case 0x08: /* Down */
                rcx_cntl( set_motor_dir, motor_dir[0]^0x80|motor_sel[0], 0, 0, mode );
                rcx_cntl( set_motor_dir, motor_dir[1]^0x80|motor_sel[1], 0, 0, mode );
                rcx_cntl( set_motor_onoff, MOTOR_ON|motor_sel[0]|motor_sel[1], 0, 0, mode );
                break;
              case 0x09: /* DownRight */
                rcx_cntl( set_motor_onoff,MOTOR_OFF|motor_sel[1], 0, 0, mode );
                rcx_cntl( set_motor_dir, motor_dir[0]^0x80|motor_sel[0], 0, 0, mode );
                rcx_cntl( set_motor_onoff, MOTOR_ON|motor_sel[0], 0, 0, mode );
                break;
              case 0x0A: /* DownLeft */
                rcx_cntl( set_motor_onoff, MOTOR_OFF|motor_sel[0], 0, 0, mode );
                rcx_cntl( set_motor_dir, motor_dir[1]^0x80|motor_sel[1], 0, 0, mode );
                rcx_cntl( set_motor_onoff, MOTOR_ON|motor_sel[1], 0, 0, mode );
                break;
              }
            }
          }
        }
      }
      if ( section == MOVEMENT ) {
        if ( cursor_x == 1 ) {
          cursor_y = save_y;
        } else if( cursor_y == 2 ) {
          cursor_y = 5;
          cursor_x = save_x;
        }
      }
      disp_message( msg_clr, black );

// Drive Mode
      while ( (cursor_y==1) && (cursor_x>0) ) {
        section = SELECTION;
        init_cursor( 3, 3, 63, 30, 40, 24 );
        move_cursor();
        if ( keycode_pre & J_A ) {
          drive_mode  = cursor_x-1;
          nvm_drive_mode = drive_mode;
          disp_joystick();
        }
      }
// Motor Select
      while ( ((cursor_y==2)||(cursor_y==3)) && (cursor_x>0) && (cursor_x<4) ) {
        section = SELECTION;
        init_cursor( 5, 5, 103, 62, 8, 8 );
        move_cursor();
        if ( keycode_pre & J_A ) {
          motor_sel[cursor_y-2] = 0x01<<cursor_x-1;
          nvm_motor_sel[cursor_y-2] = motor_sel[cursor_y-2];
          disp_joystick();
        }
      }
      while ( ((cursor_y==2)||(cursor_y==3)) && (cursor_x>3) ) {
        section = SELECTION;
        init_cursor( 6, 5, 110, 62, 8, 8 );
        move_cursor();
        if ( keycode_pre & J_A ) {
          motor_dir[cursor_y-2] = (0x40<<cursor_x-4) & 0x80;
          nvm_motor_dir[cursor_y-2] = motor_dir[cursor_y-2];
          disp_joystick();
        }
      }
// Speed
      while ( (cursor_y==4) && (cursor_x>0) ) {
        section = SELECTION;
        init_cursor( 9, 5, 87, 78, 8, 8 );
        move_cursor();
        if ( keycode_pre & J_A ) {
          motor_pwr = cursor_x-1;
          nvm_motor_pwr = motor_pwr;
          disp_joystick();
        }
      }
      if ( (section == SELECTION) && (cursor_x == 0) ) {
        save_y = cursor_y;
        cursor_y = 1;
      }
// Task
      while ( (cursor_y==5) && (cursor_x<2) ) {
        section = TASK;
        init_cursor( 3, 6, 14, 103, 16, 8 );
        move_cursor();
        if ( keycode_pre & J_A ) {
          if ( cursor_x == 0 ) {
            if ( task_num > 0 )  task_num--;
            else          task_num=9;
          } else {
            if ( task_num < 9 ) task_num++;
            else          task_num=0;
          }
          nvm_task_num = task_num;
          disp_joystick();
          waitpadup();
        }
      }
      while ( (cursor_y==5) && (cursor_x>1) ) {
        section = TASK;
        init_cursor( 4, 6, -2, 103, 24, 8 );
        move_cursor();
        if ( keycode_pre & J_A ) {
          press_button( cursor_x*3-2, 14 );
          if ( cursor_x == 2 ) {
            rcx_cntl( start_task, task_num, 0, 0, mode );
          } else {
            rcx_cntl( stop_task, task_num, 0, 0, mode );
          }
          waitpadup();
          break_button( cursor_x*3-2, 14 );
          disp_message( msg_clr, black );
        }
      }

      if ( (section==TASK) && (cursor_y==4) ) {
        cursor_y = 1;
        save_x = cursor_x;
        cursor_x = 0;
      }
    }
  }
  return( mode );
}

UBYTE mode_piano( UBYTE mode )
{
  UBYTE save_x, save_y, i;
  UWORD *song;
  UBYTE tone_len = 0x28;

  while ( 1 ) {
    mode = select_mode( mode );
    if ( mode != MODE_PIANO )  return( mode );
    cursor_y = 1;
    while ( cursor_y > 0 ) {

// Black Key
      while ( cursor_y == 1 ) {
        init_cursor( 17, 3, 18, 67, 8, 0 );
        move_cursor();
        if ( keycode_pre & J_A ) {
          if ( (cursor_x != 3)&&(cursor_x != 6)&&(cursor_x != 10)&&(cursor_x != 13) ) {
            press_key_b( cursor_x );
            rcx_cntl( play_tone, black_freq[cursor_x], black_freq[cursor_x]>>8, tone_len, mode );
            waitpadup();
            break_key_b( cursor_x );
            disp_message( msg_clr, black );
          }
        }
      }
      if ( (cursor_y == 2) && ( save_y == 3) ) {
        cursor_x = save_x;
      }

// White Key
      while ( cursor_y == 2 ) {
        save_y = 2;
        init_cursor( 18, 4, 14, 75, 8, 0 );
        move_cursor();
        if ( keycode_pre & J_A ) {
          press_key_w( cursor_x );
          rcx_cntl( play_tone, white_freq[cursor_x], white_freq[cursor_x]>>8, tone_len, mode );
          waitpadup();
          break_key_w( cursor_x );
          disp_message( msg_clr, black );
        }
      }
      if ( (cursor_y == 3) && ( save_y == 2) ) {
        save_x = cursor_x;
        cursor_x = 0;
      }

// Rest Button
      while( (cursor_y == 3) && (cursor_x == 0) ) {
        save_y = 3;
        init_cursor( 2, 5, 22, 103, 0, 0 );
        move_cursor();
        if ( keycode_pre & J_A ) {
          press_button( 1, 9 );
          rcx_cntl( play_tone, 0, 0, tone_len, mode );
          waitpadup();
          break_button( 1, 9 );
          disp_message( msg_clr, black );
        }
      }

// Length Select
      while ( (cursor_y == 3) && (cursor_x > 0) && (cursor_x < 6) ) {
        init_cursor( 7, 5, 46, 103, 8, 0 );
        move_cursor();
        if ( keycode_pre & J_A ) {
          *(UBYTE *)0xFF4F = 1;			/* select palette bank */
          set_bkg_tiles( 6, 10, 5, 1, sel_off );
          set_bkg_tiles( cursor_x+5, 10, 1, 1, sel_on );
          *(UBYTE *)0xFF4F = 0;			/* select charater bank */
          tone_len = 0xA0>>cursor_x-1;
          waitpadup();
        }
      }

// Clear & Play
      while ( (cursor_y == 3) && (cursor_x > 5) ) {
        disp_message( msg_err3, red3 );
        init_cursor( 8, 5, -26, 103, 24, 0 );
        move_cursor();
        if ( keycode_pre & J_A ) {
          press_button( cursor_x*3-5, 9 );
          waitpadup();
          break_button( cursor_x*3-5, 9 );
        }
      }
      disp_message( msg_clr, black );

// Song
      if ( cursor_y == 4 ) cursor_x = 0;
      while ( cursor_y == 4 ) {
        init_cursor( 3, 5, 22, 46, 24, 24 );
        move_cursor();
        set_bkg_tiles( 11, 14, 8, 1, msg_song[cursor_x] );
        if ( keycode_pre & J_A ) {
          press_button( cursor_x*3+1, 14 );
          if ( cursor_x==0 ) song = song1;
          if ( cursor_x==1 ) song = song2;
          if ( cursor_x==2 ) song = song3;
          i = 0;
          while ( song[i] ) {
            rcx_cntl( play_tone, song[i], song[i]>>8, song[i+1], mode );
            delay(100);
            i++;
            i++;
          }
          waitpadup();
          break_button( cursor_x*3+1, 14 );
          disp_message( msg_clr, black );
        }
      }
    }
  }
  return( mode );
}

UBYTE mode_genuine( UBYTE mode )
{
  BYTE x, y, x1, y1, x2, y2, x3, y3;
  UBYTE p1, p2, p3;
  UBYTE pad;
  UBYTE movelock = OFF;
  UBYTE i;

  for ( i=12; i<16; i++ ) {
    set_sprite_tile( i, i-4 );
    set_sprite_prop( i, 1 );
  }
  move_sprite( 12, 11*8+6, 16*8+7 );
  move_sprite( 13, 11*8+6, 17*8+7 );
  move_sprite( 14, 12*8+6, 16*8+7 );
  move_sprite( 15, 12*8+6, 17*8+7 );

  while ( 1 ) {
    mode = select_mode( mode );
    if ( mode != MODE_GENUINE )  return( mode );
    cursor_y = 1;
    while ( cursor_y > 0 ) {

// Message
      cursor_x = 0;
      while ( cursor_y == 1 ) {
        init_cursor( 3, 3, 94, 42, 24, 0 );
        move_cursor();
        if ( joypad() & J_A ) {
          set_bkg_tiles( cursor_x*3+11, 3, 1, 1, btn2_press );
          while ( joypad() & J_A ) {
            rcx_cntl( remocon, para[cursor_x]>>8, para[cursor_x], 0, mode );
          }
          rcx_cntl( remocon, 0, 0, 0, mode );
          set_bkg_tiles( cursor_x*3+11, 3, 1, 1, btn2_break );
        }
      }

// Movement
      while ( cursor_y == 2 ) {
        init_cursor( 3, 4, 94, 74, 24, 0 );
        move_cursor();
        if ( joypad() & J_SELECT ) {
          movelock = ON;
          waitpadup();
        }
        while ( (joypad()&J_A) | (movelock == ON) ) {
          if ( joypad() & J_SELECT ) {
            movelock = OFF;
            waitpadup();
          }
          if ( drive_mode == DRIVE_DS ) {
            break;
          }
          if ( motor_sel[0] == motor_sel[1] ) {
            break;
          }
          for ( i=4; i<12; i++ ) {
            set_sprite_tile( i, i%4 );
            set_sprite_prop( i, 0 );
          }
          x1 = cursor_x;
          x2 = (motor_sel[0]>>1);
          x3 = (motor_sel[1]>>1);
          y1 = y2 = y3 = 0;
          p1 = p2 = p3 = 16;
          move_cursor2( 0, x1*24+94, y1*8+74 );
          move_cursor2( 4, x2*24+94, y2*8+74 );
          move_cursor2( 8, x3*24+94, y3*8+74 );
          pad = joypad();
          while ( pad & 0xAF ) {
            pad = joypad();
            switch ( pad & 0xF0 ) {
              case 0x20:
                y1 = -1;
                p1 = 3;
                break;
              case 0x80:
                y1 = 1;
                p1 = 6;
                break;
            }
            switch ( pad & 0x0F ) {
              case 0x00:
                y2 = y3 = 0;
                p2 = p3 = 16;
                break;
              case 0x01:
                y2 = -1, y3 = 1;
                p2 = 3, p3 = 6;
                break;
              case 0x02:
                y2 = 1, y3 = -1;
                p2 = 6, p3 = 3;
                break;
              case 0x04:
                y2 = y3 = -1;
                p2 = 3, p3 = 3;
                break;
              case 0x05:
                y2 = -1, y3 = 0;
                p2 = 3, p3 = 16;
                break;
              case 0x06:
                y2 = 0, y3 = -1;
                p2 = 16, p3 = 3;
                break;
              case 0x08:
                y2 = 1, y3 = 1;
                p2 = 6, p3 = 6;
                break;
              case 0x09:
                y2 = 1, y3 = 0;
                p2 = 6, p3 = 16;
                break;
              case 0x0A:
                y2 = 0, y3 = 1;
                p2 = 16, p3 = 6;
                break;
            }
            move_cursor2( 0, x1*24+94, y1*8+74 );
            move_cursor2( 4, x2*24+94, y2*8+74 );
            move_cursor2( 8, x3*24+94, y3*8+74 );
            if ( y1 != 0 ) set_bkg_tiles( x1*3+11, y1+7, 1, 1, btn2_press );
            if ( y2 != 0 ) set_bkg_tiles( x2*3+11, y2+7, 1, 1, btn2_press );
            if ( y3 != 0 ) set_bkg_tiles( x3*3+11, y3+7, 1, 1, btn2_press );
            rcx_cntl( remocon, (para[x1+p1] | para[x2+p2] | para[x3+p3])>>8,
                                para[x1+p1] | para[x2+p2] | para[x3+p3], 0, mode );
          }
          for ( y=0; y<2; y++ ) {
            for ( x=0; x<3; x++ ) {
              set_bkg_tiles( x*3+11, y*2+6, 1, 1, btn2_break );
            }
          }
        }
        for ( i=4; i<12; i++ ) {
          set_sprite_tile( i, 4 );
          set_sprite_prop( i, 1 );
        }
      }

// Task
      cursor_x = 0;
      while ( cursor_y == 3 ) {
        init_cursor( 2, 5, 94, 106, 48, 0 );
        move_cursor();
        if ( joypad() & J_A ) {
          set_bkg_tiles( cursor_x*6+11, 11, 1, 1, btn2_press );
          while ( joypad() & J_A ) {
            rcx_cntl( remocon, para[cursor_x+9]>>8, para[cursor_x+9], 0, mode );
          }
          rcx_cntl( remocon, 0, 0, 0, mode );
          set_bkg_tiles( cursor_x*6+11, 11, 1, 1, btn2_break );
        }
      }
      cursor_x = 0;
      while ( cursor_y == 4 ) {
        init_cursor( 3, 6, 94, 122, 24, 0 );
        move_cursor();
        if ( joypad() & J_A ) {
          set_bkg_tiles( cursor_x*3+11, 13, 1, 1, btn2_press );
          while ( joypad() & J_A ) {
            rcx_cntl( remocon, para[cursor_x+11]>>8, para[cursor_x+11], 0, mode );
          }
          rcx_cntl( remocon, 0, 0, 0, mode );
          set_bkg_tiles( cursor_x*3+11, 13, 1, 1, btn2_break );
        }
      }
      cursor_x = 0;
      while( cursor_y == 5 ) {
        init_cursor( 2, 6, 94, 58, 48, 16 );
        move_cursor();
        if ( joypad() & J_A ) {
          set_bkg_tiles( cursor_x*6+11, 15, 1, 1, btn2_press );
          while ( joypad() & J_A ) {
            rcx_cntl( remocon, para[cursor_x+14]>>8, para[cursor_x+14], 0, mode );
          }
          rcx_cntl( remocon, 0, 0, 0, mode );
          set_bkg_tiles( cursor_x*6+11, 15, 1, 1, btn2_break );
        }
      }
    }
  }
  return( mode );
}

void main()
{
  UBYTE i, j, mode;

  init_character();

  /* initialize bank number */
  ENABLE_RAM_MBC1;
  SWITCH_RAM_MBC1( 0 );
  if ( joypad() & J_START ) {
    nvm_status = 0xFF;
    set_bkg_tiles( 2, 7, 16, 1, msg_default );
    set_bkg_tiles( 7, 9, 6, 1, msg_pusha );
    waitpad(J_A);
  }
  if ( nvm_status != 0x55 ) {
    nvm_status = 0x55;
    nvm_drive_mode = DRIVE_LR;
    nvm_motor_sel[0] = MOTOR_A;
    nvm_motor_sel[1] = MOTOR_C;
    nvm_motor_dir[0] = REVERSE;
    nvm_motor_dir[1] = REVERSE;
    nvm_motor_pwr = 4;
    nvm_task_num = 0;
  }
  drive_mode = nvm_drive_mode;
  motor_sel[0] = nvm_motor_sel[0];
  motor_sel[1] = nvm_motor_sel[1];
  motor_dir[0] = nvm_motor_dir[0];
  motor_dir[1] = nvm_motor_dir[1];
  motor_pwr = nvm_motor_pwr;
  task_num = nvm_task_num;
  motor_pwr_old = motor_pwr;
  cursor_x = 0;
  mode = MODE_JOYSTICK;
  while( 1 ) {
    cls();
    set_bkg_data( 0, 128, tile[mode] );
    set_bkg_tiles( 0, 0, 20, 18, form[mode] );
    *(UBYTE *)0xFF4F = 1;			/* select palette bank */
    set_bkg_tiles( 0, 0, 20, 18, form_a[mode] );
    *(UBYTE *)0xFF4F = 0;			/* select charater bank */
    switch( mode ) {
      case MODE_JOYSTICK:
        mode = mode_joystick( mode );
        break;
      case MODE_PIANO:
        mode = mode_piano( mode );
        break;
      case MODE_GENUINE:
        mode = mode_genuine( mode );
        break;
    }
  }
}

/* EOF */